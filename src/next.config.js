const withPlugins = require("next-compose-plugins");
const withSass = require('@zeit/next-sass')
const withSize = require('next-size');
const webpack = require('webpack')

const { parsed: localEnv } = require('dotenv').config()

module.exports = withPlugins(
  [
    withSass,
    withSize
  ],
  {

    distDir: '../build',

    // Environment Vars
    webpack: (config) => {
      config.plugins.push(
        new webpack.EnvironmentPlugin(localEnv)
      )
      return config
    }
  }
);