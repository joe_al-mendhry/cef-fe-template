import React, { Component } from 'react'
import Header from "../Header/Header";
import Meta from "../Meta";

// import './_layout.scss';

export default class Layout extends Component {
  render() {
    return (
      <React.Fragment>
        <Meta />
        <div className="container">
          <Header />
          {this.props.children}
        </div>
      </React.Fragment>
    )
  }
}
