import React, { Component } from 'react'
import ReCAPTCHA from "react-google-recaptcha";
import Link from 'next/link';

export default class About extends Component {

  // Get Query Params
  static getInitialProps({ query }) {
    return { query }
  }
  constructor(props) {
    super(props);

    this.state = {
      siteKey: process.env.RECAPTCHA_SITE_KEY
    };
  }

  onChange = () => {
    console.log("success");
  }

  render() {
    return (
      <React.Fragment>
        <h1>This is the About Us Page</h1>

        <Link href="/" as="/">
          <a>Go HOME</a>
        </Link>

        <form method="post" action="about-us/submit">
          <input type="text" placeholder="hello" />
          <ReCAPTCHA
            sitekey={this.state.siteKey}
            onChange={this.onChange}
          />
          <input type="submit" />
        </form>
      </React.Fragment>
    )
  }
}
