import React, { Component } from 'react'
import Link from 'next/link';
import Router from 'next/router';

export default class Home extends Component {
  render() {
    return (
      <React.Fragment>
        <h1>Hello world</h1>

        <Link as={`/about-us`} href={`/about-us`}>
          <a>Go To The About Page</a>
        </Link>

      </React.Fragment>
    )
  }
}
