require('dotenv').config();
const path = require('path');
const express = require('express');
const next = require('next');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const initRoutes = require('./routes')

const isDev = process.env.NODE_ENV !== 'production'
const PORT = process.env.PORT || 5000;

const app = next({ dev: isDev, dir: './src' })
const handle = app.getRequestHandler()

app.prepare()
  .then(() => {
    const server = express();
    server.use(bodyParser.json());
    server.use(bodyParser.urlencoded({ extended: false }));
    server.use(cookieParser());
    if (isDev) {
      server.use(logger('dev'));
    }

    initRoutes(server, app);

    // If none of the custom routing handlers from express are hit,
    // defer to next's own handler.
    server.get('*', (req, res) => {
      return handle(req, res)
    });

    server.listen(PORT, (err) => {
      if (err) throw err
      console.log(`> Ready on http://localhost:${PORT}`)
    });
  })
  .catch((ex) => {
    console.error(ex.stack)
    process.exit(1)
  });