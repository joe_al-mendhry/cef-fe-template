const aboutRoutes = require('./about-us/aboutRoutes');
const apiRoutes = require('./api/apiRoutes');

module.exports = (server, app) => {
  aboutRoutes(server, app)
  apiRoutes(server);
};