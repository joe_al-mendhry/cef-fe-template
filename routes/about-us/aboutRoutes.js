const express = require('express'),
  Recaptcha = require('express-recaptcha').RecaptchaV2,
  RECAPTCHA_SITE_KEY = process.env.RECAPTCHA_SITE_KEY,
  RECAPTCHA_SECRET_KEY = process.env.RECAPTCHA_SECRET_KEY

const recaptcha = new Recaptcha(RECAPTCHA_SITE_KEY, RECAPTCHA_SECRET_KEY);

/**
 * /about-us Routes
 * @param {} server - Express Server
 * @param {} app - NextJS APP
 */
module.exports = (server, app) => {
  /*
  * Clean URL Example
  * https://nextjs.org/learn/basics/server-side-support-for-clean-urls
  
    server.get('/about-us', recaptcha.middleware.render, (req, res) => {
      const actualPage = '/about-us';
      const queryParams = { recaptcha: res.recaptcha };
      return app.render(req, res, actualPage, queryParams);
    });
  */

  server.post('/about-us/submit', recaptcha.middleware.verify, (req, res) => {
    if (req.recaptcha.error) {
      res.redirect('/about-us?error=recaptcha')
    } else {
      res.redirect('/?success');
    }
  });
}