# An Express / NextJS Setup


## Run locally
```
npm install
npm run dev
```

## Production
```
npm run build
npm run start
```

## Environment Vars
Rename `.env-example` to `.env` and fill out the correct values